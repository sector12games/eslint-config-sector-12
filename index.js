module.exports = {
    'extends': 'airbnb-base',
    'plugins': [
        'import'
    ],
    'rules': {
        'arrow-body-style': ['error', 'always'],
        'func-names': ['error', 'never'],
        'import/no-extraneous-dependencies': ['error', { 'devDependencies': true }],
        'indent': ['error', 4, { 'SwitchCase': 1 }],
        'linebreak-style': ['off'],
        'no-console': 0,
        'no-unused-vars': ['error', { 'argsIgnorePattern': 'result|error' }],
        'no-use-before-define': ['error', { 'functions': false, 'classes': true }],
        'spaced-comment': 0,
        'wrap-iife': ['error', 'inside'],
        'valid-jsdoc': ['error']
    }
};